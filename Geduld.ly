\version "2.20.0"
\language "deutsch"
\header { tagline = "" }
\paper {
  indent = 0\mm
  line-width = 120\mm
  oddFooterMarkup = ##f
  oddHeaderMarkup = ##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}		   
{
  \clef bass
  a8( c') h( e) a( c') h( e) |
  a16. c'32 g16. c'32 fis16. c'32 d16. c'32
  g16. a32 b16. g32 cis'16. g32 e'16. g32 |
  f!16. a32 e16. a32 dis16. a32 h,16. a32
  e16. fis32 g16. e32 b16. e32 cis'16.e32 |
  d16. f!32 c16. f32 h,16. f32 a,16. f32 gis,16. e32 c16. a,32 e8 e, |
}  