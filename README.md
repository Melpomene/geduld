# Geduld

![35. Aria](Geduld.png)

Continuo voice from BWV 244, 35 Aria [(NBA)](http://imslp.eu/files/imglnks/euimg/7/7d/IMSLP580710-PMLP3301-bachNBAII,5matthaeus-passionBWV244.pdf) engraved with [Lilypond](https://lilypond.org/)

Create png file with 
```
lilypond -dbackend=eps -dno-gs-load-fonts -dinclude-eps-fonts --png Geduld.ly
```
